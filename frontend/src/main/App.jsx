import '../common/template/dependencies'
import React from 'react'
import Header from '../common/template/header.jsx'
import Sidebar from "../common/template/sidebar.jsx"
import Footer from '../common/template/footer.jsx'
import Routes from './routes.jsx'

export default props =>(
    <div className="wrapper">
        <Header/>
        <Sidebar/>
        <div className="content-wrapper">
            <Routes/>
        </div>
        <Footer/>
    </div>
)