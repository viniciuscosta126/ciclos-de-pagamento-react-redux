import React, { Component } from "react";
import ContentHeader from "../common/template/contentHeader.jsx";
import Content from "../common/template/content.jsx";
import ValueBox from "../common/widget/valueBox.jsx";
import Row from "../common/layout/row.jsx";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getSumarry } from "./dashboardActions.js";

class Dashboard extends Component {
    componentWillMount(){
        this.props.getSumarry()
    }
  render() {
    const { credit, debt } = this.props.summary;
    return (
      <div>
        <ContentHeader title="Dashboard" small="Versao 1.0.0" />
        <Content>
          <Row>
            <ValueBox
              cols="12 4"
              color="green"
              icon="bank"
              value={`R$${credit}`}
              text="Total creditos"
            />
            <ValueBox
              cols="12 4"
              color="red"
              icon="credit-card"
              value={`R$${debt}`}
              text="Total de Debitos"
            />
            <ValueBox
              cols="12 4"
              color="blue"
              icon="money"
              value={`R$${credit - debt}`}
              text="Valor Consolidado"
            />
          </Row>
        </Content>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  summary: state.dashboard.summary,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ getSumarry }, dispatch);

export default connect(mapStateToProps,mapDispatchToProps)(Dashboard);
