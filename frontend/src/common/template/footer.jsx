import React from 'react'

export default props=>(

    <footer className="main-footer">
        <strong>
            Copyright &copy; 2021
            <a href="#vinicius" target="_blank"> Vinicius Costa</a>
        </strong>
    </footer>

)